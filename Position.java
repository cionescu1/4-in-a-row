//A Position object is denoted by the current column and
//the first available row

class Position
{
    private int row, col;

    //Create a Position with the given coordinates
    public Position(int r, int c)
    {
        col = c;
        row = r;
    }

    //Return the row number
    public int row()
    {
        return row;
    }

    //Return the column number
    public int col()
    {
        return col;
    }
}
