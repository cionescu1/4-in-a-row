import javax.swing.*;
import java.awt.*;
import java.awt.Image;
import java.awt.event.*;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.util.Scanner;
import java.util.*;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.text.*;
import javax.swing.*;
import java.awt.image.BufferedImage;

class DrawPanel extends JPanel
{
    private int row;
    private int col;
    private Image icon  = new ImageIcon(this.getClass()
        .getResource("white.png")).getImage();
    private int timeCount = 0;
    private String timeString;
    private int timeActual= 0;

    public DrawPanel(int r, int c)
    {
        row = r;
        col = c;
        setBackground(Color.WHITE);
    }

    public void animate()
    {
        try
        {
            Thread.sleep(1);
        }
        catch(Exception e)
        {}
        repaint();
        timeCount += 1;
        if(timeCount >= 400)
        {
            timeActual += 1;
            timeCount = 0;
        }
        timeString = "Time: " + Integer.toString((Integer)(timeActual/60))+" min "+Integer.toString((Integer)(timeActual%60))+ " sec ";
    }

    public void paintComponent(Graphics g)
    {
        for(int i = 0; i < row + 2; i++)
        {
            for(int j = 0; j < col + 2; ++j)
            {
                g.drawImage(icon, (i)*50, (j)*50, null);
            }
        }
        Font font = new Font("Serif", Font.PLAIN, 20);
        g.setFont(font);
        g.drawString( timeString, 50, 20);
        setBackground(Color.WHITE);
        for(int i = 0; i < row ; ++i)
        {
            for(int j = 0; j <= col; ++j)
            {
                g.drawImage(Remember.image[j][i], (i+1)*50, (j+1)*50, null);
            }
        }
        animate();
    }
}
