//Provide constants Player.One and Player.Two for the two players and constants
//Player.None and Player.Both to represent neither player or both players.

enum Player
{
    One, Two, None, Both;

    //Returns the other player from the current one
    public Player other()
    {
        if(this == One)
            return Two;
        else if (this == Two)
            return One;
        else
            throw new Error("Bug: other() called on None or Both");
    }
}
