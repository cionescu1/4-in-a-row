import javax.swing.*;
import java.awt.*;
import java.awt.Image;
import java.awt.event.*;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.util.Scanner;
import java.util.*;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.text.*;
import javax.swing.*;

class Interface1 implements ActionListener, KeyListener
{
    private JButton[][] choices = new JButton[4][4];
    private static int r;
    private static int c;
    private int size=0;
    private JButton instruction;
    private JFrame w;
    private boolean ok = true;
    private JPanel panel1 = new JPanel();
    private JPanel panel;
    private static JFrame p;
    private Box theBox = null;
    private boolean create = true;
    private DrawPanel thePanel;
    private Remember remember;
    private Player player = Player.One;
    private JFrame ins;
    private JFrame winner;
    private ImageIcon icon0  = new ImageIcon(this.getClass().getResource("win.png"));
    private ImageIcon icon1  = new ImageIcon(this.getClass().getResource("lost.png"));
    private static ImageIcon i0;
    private static ImageIcon i1;
    private static JTextField time = new JTextField("Time: 0");
    private static int timeCount = 0;
    private ImageIcon draw = new ImageIcon(this.getClass().getResource("draw.png"));
    private static ImageIcon d;

    //Constructor creates the main window with the menu and then the other frames we use
    public Interface1()
    {
        d = draw;
        i0 = icon0;
        i1 = icon1;
        w = new JFrame("4-in-a-row");
        w.setDefaultCloseOperation(w.EXIT_ON_CLOSE);
        w.setLocation(100, 100);
        w.setBackground(Color.WHITE);
        ImageIcon icon = new ImageIcon(this.getClass()
            .getResource("logo.png"));

        JLabel label = new JLabel(icon);
        GridLayout grid = new GridLayout(3, 2);
        panel = new JPanel();
        panel.add(label);
        panel.add(display());

        ins = new JFrame("Instructions");
        ins.setBackground(Color.WHITE);
        ins.setLocation(100, 100);
        ins.setDefaultCloseOperation(w.EXIT_ON_CLOSE);
        ins.setResizable(false);

        instruction = new JButton("Instructions");

        Box box = Box.createHorizontalBox();
        JLabel label2 = new JLabel();
        box.add(instruction);
        panel.add(box);

        instruction.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent f)
            {
                Box box = Box.createHorizontalBox();
                ImageIcon icon0 = new ImageIcon(this.getClass()
                    .getResource("instructions.png"));
                JLabel label0 = new JLabel(icon0);
                box.add(label0);
                w.setVisible(false);
                JButton back = new JButton("Play");
                Box backBox = Box.createHorizontalBox();
                backBox.add(back);
                Box bigBox = Box.createVerticalBox();
                bigBox.add(box);
                bigBox.add(backBox);
                JPanel p = new JPanel();
                p.add(bigBox);
                p.setBackground(Color.WHITE);
                ins.add(p);
                ins.setVisible(true);
                ins.pack();
                back.addActionListener(new ActionListener()
                {
                    public void actionPerformed(ActionEvent h)
                    {
                        ins.setVisible(false);
                        try
                        {
                            Thread.sleep(100);
                        }
                        catch(Exception j)
                        {}
                        w.setVisible(true);
                    }
                });
            }
        });

        panel.setLayout(grid);
        panel.setBackground(Color.WHITE);

        w.add(panel);
        w.setResizable(false);
        w.pack();
        w.setVisible(true);
        p = new JFrame("4-in-a-row");
        p.setBackground(Color.WHITE);
        p.setLocation(100, 100);
        p.setDefaultCloseOperation(w.EXIT_ON_CLOSE);
        p.setResizable(false);
        p.addKeyListener(this);
    }

    //Returns the row number
    public int row()
    {
        return r;
    }

    //Returns the column number
    public int col()
    {
        return c;
    }

    //returns the position of the red ball
    public int getSize()
    {
        return size;
    }

    void set(int row, int col)
    {
        r = row;
        c = col;
    }

    //Creates a box containing the buttons from the menu
    Box display()
    {
        Box box = Box.createHorizontalBox();
        box.add(choices());
        return box;
    }

    //Creates an array of buttons representing all the combinations of sizes
    Box choices()
    {
        Box box = Box.createHorizontalBox();
        JTextPane textPane = new JTextPane();
        textPane.setText("Play\nSelect the board\ndimensions:");
        StyledDocument doc = textPane.getStyledDocument();
        SimpleAttributeSet bSet = new SimpleAttributeSet();
        StyleConstants.setAlignment(bSet, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0, doc.getLength(), bSet, false);
        textPane.setEditable(false);
        box.add(textPane);

        Box[] cols = new Box[4];
        for(int i = 0; i < 4; ++i)
        {
            cols[i] = Box.createVerticalBox();
            for(int j = 0; j < 4; ++j)
            {
                choices[i][j] = new JButton((i+6) + " x " + (j+6));
                choices[i][j].addActionListener(this);
                cols[i].add(choices[i][j]);
                choices[i][j].setActionCommand((i+6) + " x " + (j+6));
            }
            box.add(cols[i]);
        }
        return box;
    }

    public void actionPerformed(ActionEvent e)
    {
        JTextField text = new JTextField();
        Box box = Box.createHorizontalBox();
        String action = e.getActionCommand();
        for(int i = 0; i < 4; ++i)
        {
            for(int j = 0; j < 4; ++j)
            {  
                if(action.equals((i+6) + " x " + (j+6)))
                {
                    w.setVisible(false);
                    set(i+6, j+6);
                    p.setSize(new Dimension((c+2)*50, (r+3)*50));
                    p.setVisible(true);
                    thePanel = new DrawPanel(c, r);
                    p.setBackground(Color.WHITE);
                    p.add(thePanel);
                    Play.startGame();
                    remember = new Remember(r, c);
                    thePanel.animate();
                    return;
                }
            }
        }
    }

    public void keyPressed(KeyEvent e)
    {
    }

    public void keyReleased(KeyEvent e)
    {
        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT)
        {
            if(size > 0)
            {
                size--;
                thePanel.animate();
                Remember.modifyImage(0, size, player);
            }
            else
            {
                size = 0;
            }
        }

        if (key == KeyEvent.VK_RIGHT)
        {
            if(size < c-1)
            {
                size++;
                thePanel.animate();
                Remember.modifyImage(0, size, player);
            }
            else
            {
                size = c-1;
            }
        }

        if (key == KeyEvent.VK_ENTER)
        {
            Play.play(size);
        }
    }

    public void keyTyped(KeyEvent e)
    {
    }

    public void removeListener()
    {
        p.removeKeyListener(this);
    }

    public void endGame(Player win)
    {
        winner = new JFrame ("Winner");
        Box box = Box.createHorizontalBox();
        JLabel label3;
        if(win == Player.One)
        {
            label3 = new JLabel(i0);
        }
        else
            if(win == Player.Two)
            {
                label3 = new JLabel(i1);
            }
            else
                {
                    label3 = new JLabel(d);
                }
        box.add(label3);
        winner.add(box);
        p.removeKeyListener(this);
        winner.setVisible(true);
        winner.pack();
        winner.setLocation(100+(c+2)*50,100);
        winner.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        winner.setResizable(false);
    }
}
