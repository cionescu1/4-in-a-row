//Will create a board and make a move on it, suggest a move (for computer)

import java.util.*;

class Board
{
    private Player[][] board;
    private Player current;
    private int dimX, dimY;

    //Creates a board, with 1 to play next
    public Board(int x, int y)
    {
        dimX = x;
        dimY = y;
        board = new Player[x][y];
        for(int i = 0; i < dimX; i ++)
        {
            for(int j = 0; j < dimY; j++)
            {
                board[i][j] = null;
            }
        }
        current = Player.One;
    }

    //Returns current player
    public Player current()
    {
        return current;
    }

    //Returns dimX
    public int row()
    {
        return dimX;
    }

    //Returns dimY
    public int col()
    {
        return dimY;
    }

    //Takes a column from the human player and converts it into a position
    //(by finding the first available row on the specified column),
    //returning Player.None if it is not valid
    Position position(int c)
    {
        for(int i = dimX - 1; i >= 0; i--)
        {
            if(board[i][c] == null)
            {
                Position pos = new Position(i, c);
                return pos;
            }
        }
        return null;
    }

    //Takes a position object and makes a move in that position,
    //according to who is the current player
    void move(Position pos)
    {
        int r = pos.row(), c = pos.col();
        if(current == Player.One)
            board[r][c] = Player.One;
        else
            board[r][c] = Player.Two;
        current = current.other();
    }

    //Returns the number of occupied positions in the board
    int full()
    {
        int nr = 0;
        for(int i = 0; i < dimX; i++)
        {
            for(int j = 0; j < dimY; j++)
            {
                if(board[i][j] != null)
                    nr++;
            }
        }
        return nr;
    }

    //Returns the player who is the winner in the current board state,
    //or Player.None if the game isn't yet over, or Player.Both if it is a draw
    Player winner()
    {
        //check vertical spaces
        for(int i = 0; i < dimX - 3; i++)
        {
            for(int j = 0; j < dimY; j++)
            {
                if(board[i][j] != null
                    && board[i][j] == board[i+1][j]
                    && board[i+1][j] == board[i+2][j]
                    && board[i+2][j] == board[i+3][j])
                {
                    Remember.makeFinish(i, j, i+1, j, i+2, j, i+3, j, board[i][j]);
                    return board[i][j];
                }
            }
        }
        //check horizontal spaces
        for(int i = 0; i < dimX; i++)
        {
            for(int j = 0; j < dimY - 3; j++)
            {
                if(board[i][j] != null
                    && board[i][j] == board[i][j+1]
                    && board[i][j+1] == board[i][j+2]
                    && board[i][j+2] == board[i][j+3])
                {
                    Remember.makeFinish(i, j, i, j+1, i, j+2, i, j+3, board[i][j]);
                    return board[i][j];
                }
            }
        }
        //check / diagonal spaces
        for(int i = 0; i < dimX - 3; i++)
        {
            for(int j = 3; j < dimY; j++)
            {
                if(board[i][j] != null
                    && board[i][j] == board[i+1][j-1]
                    && board[i+1][j-1] == board[i+2][j-2]
                    && board[i+2][j-2] == board[i+3][j-3])
                {
                    Remember.makeFinish(i, j, i+1, j-1, i+2, j-2, i+3, j-3, board[i][j]);
                    return board[i][j];
                }
            }
        }
        //check \ diagonal spaces
        for(int i = 0; i < dimX - 3; i++)
        {
            for(int j = 0; j < dimY - 3; j++)
            {
                if(board[i][j] != null
                    && board[i][j] == board[i+1][j+1]
                    && board[i+1][j+1] == board[i+2][j+2]
                    && board[i+2][j+2] == board[i+3][j+3])
                {
                    Remember.makeFinish(i, j, i+1, j+1, i+2, j+2, i+3, j+3, board[i][j]);
                    return board[i][j];
                }
            }
        }
        if(full() == dimX*dimY) return Player.Both;
        return Player.None;
    }

    //Returns an array of blank positions so that the main program
    //can choose between them to make its own move
    Position[] blanks()
    {
        List<Position> theList = new ArrayList<Position>();
        Position pos;
        for(int i = dimX - 1; i >= 0; i--)
        {
            for(int j = 0; j < dimY; j++)
            {
                if((i == dimX - 1) && (board[i][j] == null))
                {
                    pos = new Position(i,j);
                    theList.add(pos);
                    continue;
                }
                if(board[i][j] == null && board[i+1][j] != null)
                {
                    pos = new Position(i, j);
                    theList.add(pos);
                    continue;
                }
            }
        }
        Position[] blanks = new Position[theList.size()];
        theList.toArray(blanks);
        return blanks;
    }

    //Returns a string representation of the board
    public String toString()
    {
        String display = new String();
        for(int i = 0; i < dimX; i++)
        {
            for(int j = 0; j < dimY; j++)
            {
                if(board[i][j] == Player.One)
                    display += "O";
                else if(board[i][j] == Player.Two)
                    display += "T";
                else if(board[i][j] == null)
                    display += "-";
            }
            display += "\n";
        }
        return display;
    }

    boolean fullUntil(int r, int c)
    {
        for(int i = dimX - 1; i > r; i--)
        {
            if(board[i][c] == null)
                return false;
        }
        return true;
    }

    //Returns a single suggested position
    Position suggest()
    {
        Position p;
        for(int i = dimX - 2; i > 0; i--)
        {
            for(int j = 0; j < dimY; j++)
            {
                if(board[i][j] != null && board[i][j] == board[i+1][j])
                {
                    Position pos = new Position(i-1, j); 
                    if(board[i-1][j] == null)      
                        return pos;
                }
            }
        }
        for(int i = dimX - 1; i >= 0; i--)
        {
            for(int j = 1; j < dimY - 1; j++)
            {
                if(board[i][j] != null && board[i][j-1] == board[i][j])
                {
                    Position pos = position(j+1);
                    if(pos != null && pos.row() == i)
                        return pos;
                }
            }
        }
        for(int i = dimX - 1; i >= 0; i--)
        {
            for(int j = 1; j < dimY - 2; j++)
            {
                if(board[i][j] != null && board[i][j] == board[i][j+1])
                {
                    Position pos = position(j-1);
                    if(pos != null && pos.row() == i)
                        return pos;
                }
           }
        }
        for(int i = dimX - 2; i > 0; i--)
        {
            for(int j = 1; j < dimY - 2; j++)
            {
                if(board[i][j] != null && board[i][j] == board[i+1][j-1])
                {
                    Position pos = position(j+1);
                    if(pos != null  && fullUntil(i-1, j+1) == true)
                        return pos;
                }
            }
        }
        return null;
    }

    //Function used for testing
    public static void main(String[] args)
    {
        /*Board board = new Board(8, 9);
        Position[] blanks = board.blanks();
        board.move(new Position(board.row()-1, 0));
        board.move(new Position(board.row()-1, 1));
        board.move(new Position(board.row()-2, 1));
        board.move(new Position(board.row()-2, 0));
        board.move(new Position(board.row()-3, 2));
        board.move(new Position(board.row()-3, 1));
        board.move(new Position(board.row()-4, 3));
        System.out.println(board.toString());
        Player p = board.winner();
        if(p == Player.None)
            System.out.println("None");
        if(p == Player.One)
            System.out.println("One");
        if(p == Player.Two)
            System.out.println("Two");*/
    }
}
