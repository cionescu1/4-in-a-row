//Main class, plays the 4-in-a-row game

import java.util.Scanner;
import javax.swing.*;
import java.awt.*;
import java.util.Random;

class Play implements Runnable
{
    //A random number generator is used to pick the computer's moves
    private static Board board;
    private Random gen;
    private Interface1 interface1;
    private static Play program;

    //Start the game
    public static void main(String[] args)
    {
        program = new Play();
        SwingUtilities.invokeLater(program);
    }

    public void run()
    {
        try
        {
            interface1 = new Interface1();
        }
        catch(Exception e)
        {
            System.out.println("Exception for GUI " + e);
        }    
    }

    public static void startGame()
    {
        program.board = new Board(program.interface1.row(), program.interface1.col());
    }

    //Initialise and loop through pairs of moves, one by the human player (who
    //plays first and is Player One) and one by the computer player
    public static void play(int x)
    {
        boolean ok = program.humanMove(x);
        if(ok == false)
            return;
        Player winner = program.board.winner();
        if(winner != Player.None)
        {
            program.stop(winner);
            return;
        }
        program.computerMove();
        winner = program.board.winner();
        if(winner != Player.None)
        {
            program.stop(winner);
            return;
        }
        Remember.modifyImage(0,x,Player.One);
    }

    public static int possibleMove(int x)
    {
        Position pos = board.position(x);
        if(pos == null)
            return -1;
        return pos.row() + 1;
    }

    //Make a move by the human
    boolean humanMove(int x)
    {
        Position pos = board.position(x);
        if(pos == null)
            return false;
        board.move(pos);
        for(int i = 0; i < pos.row() + 1; i++)
        {
            Remember.modifyImage(i+1, x, board.current());
        }
        return true;
    }

    //Make a move by the computer
    void computerMove()
    {
        Position p = board.suggest();
        if(p == null)
        {
            Position[] blanks = board.blanks();
            gen = new Random();
            int index = gen.nextInt(blanks.length);
            Position pos = blanks[index];
            board.move(pos);
            Remember.modifyImage(pos.row()+1, pos.col(), board.current());
        }
        else
        {
            board.move(p);
            Remember.modifyImage(p.row()+1, p.col(), board.current());
        }
    }

    //Check whether one player has won, print and stop the game if so
    private void stop(Player winner)
    {
        if(winner == Player.One)
            interface1.endGame(Player.One);
        else if(winner == Player.Two)
            interface1.endGame(Player.Two);
        else interface1.endGame(Player.Both);
    }
}
